
sealed case class Animal
case class Dog extends Animal
case class Cat extends Animal

object Joker {
    
}

object BlahMain {
  def main(args: Array[String]): Unit = {
    Cat match {
      case `Cat` => println("cat")
    }
  }
}